init:
	pip install -r requirements.txt

clean:
	find . -name "*.pyc" -delete
	rm -f svc_discovery_lb.tar.gz

archive:
	git archive -o svc_discovery_lb.tar.gz --prefix=svc_discovery_lb/ HEAD
