# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='svc_discovery_lb',
    version='0.1.0',
    description='Use Service Discovery tools to configure a Load-Balancer',
    long_description=readme,
    author='Baptiste Assmann',
    author_email='bedis9@gmail.com',
    url='https://www.bedis9.net/',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

