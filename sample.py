from sdlb.sdlb import sdlb
from os import path
import os

# example with kubernetes
import kubernetes.client

kubeconf = kubernetes.client.configuration
kubeconf.host = 'https://kube-controller1.kvm'
kubeconf.verify_ssl = False

if path.isfile('/var/run/secrets/kubernetes.io/serviceaccount/ca.crt'):
    kubeconf.ssl_ca_cert = '/var/run/secrets/kubernetes.io/serviceaccount/ca.crt'
    kubeconf.verify_ssl = True

if path.isfile('/var/run/secrets/kubernetes.io/serviceaccount/token'):
    with open('/var/run/secrets/kubernetes.io/serviceaccount/token', 'r') as tokenfile:
        mytoken = tokenfile.read().replace('\n', '')
    kubeconf.api_key['authorization'] = mytoken
    kubeconf.api_key_prefix['authorization'] = 'Bearer'
else:
    kubeconf.cert_file = './admin.pem'
    kubeconf.key_file = './admin-key.pem'

if os.getenv('KUBE_APISERVER'):
    kubeconf.host = os.getenv('KUBE_APISERVER')

lbName = 'vtm-ingress'
if os.getenv('KUBE_LBNAME'):
    lbName = os.getenv('KUBE_LBNAME')

lbApiUrl = 'http://127.0.0.1:9070/'
if os.getenv('KUBE_LBAPIURL'):
    lbApiUrl = os.getenv('KUBE_LBAPIURL')

lbApiPassword = 'admin'
if os.getenv('KUBE_LBAPIPASSWORD'):
    lbApiPassword = os.getenv('KUBE_LBAPIPASSWORD')

lbDriver = 'haproxy'
if os.getenv('LBDRIVER'):
    lbDriver = os.getenv('LBDRIVER')

mylb = sdlb('kubernetes', kubeconf, lbDriver, lbName)
if lbDriver == 'vtm':
    mylb.lb.driver.init_vtm_api(lbApiUrl, 'admin', lbApiPassword)

mylb.debug = True
kubernetes.client.configuration = mylb.sdconfiguration
mylb.start()
