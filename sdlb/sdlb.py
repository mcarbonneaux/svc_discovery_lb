from sdkubernetes import kubelb, watchIngresses
import threading

class sdlb:
    """ Description """
    sdtype = ''
    sdconfiguration = ''
    lbidentifier = ''
    conf_buffer = 5
    lb = ''
    lbtype = ''
    debug = False
    # threads for this configuration
    threads = {}

    def __init__(self, sdtype, configuration, lbtype, lbname):
        self.sdtype = sdtype
        self.sdconfiguration = configuration
        self.lbidentifier = lbname
        self.lbtype = lbtype
        self.conf_buffer = 5
        self.debug = False

        if self.sdtype == 'kubernetes':
            self.lb = kubelb(self.lbidentifier, self.lbtype)
        else:
            print('service discovery %s not supported yet' % self.sdname)

    def start(self):
        if self.sdtype == 'kubernetes':
            t = threading.Thread(target=watchIngresses(self.lbidentifier, self))
            self.threads[self.lbidentifier] = t
            t.start()
        else:
            print('service discovery %s not supported yet' % self.sdname)

