from lbhaproxy import haproxydriver
from lbvtm import vtmdriver
import kubernetes.client
from kubernetes import watch
from kubernetes.client.rest import ApiException
import threading
import datetime
import time

class kubelbserver:
    serverName = ''
    serverIP = ''
    podName = ''

    def __init__(self, server_name, server_ip):
        self.serverName = server_name
        self.serverIP = server_ip
        self.podName = ''

    def rm(self):
        self.serverName = ''
        self.serverIP = ''
        self.podName = ''


class kubelbpath:
    path = ''
    serviceName = ''
    servicePort = ''
    serverResolution = '' # could be DNS or RC
    servers = []
    owner = ''

    def __init__(self, path, service_name, service_port, owner):
        self.path = path
        self.serviceName = service_name
        self.servicePort = service_port
        self.serverResolution = 'RC'
        self.servers = []
        self.owner = owner

    def add_server(self, srv):
        if not self.servers:
            self.servers.append(srv)
            return

        for server in self.servers:
            if srv.serverIP == server.serverIP:
                return

        self.servers.append(srv)

    def clear_serverlist(self):
        self.servers = []

    def rm(self):
        self.owner.lb.driver.rm_backend(self.serviceName)
        self.path = ''
        self.serviceName = ''
        self.servicePort = ''
        self.serverResolution = ''
        self.owner = ''
        for server in self.servers:
            server.rm()


class kubelbrule:
    host = ''
    paths = []
    owner = ''

    def __init__(self, host, owner):
        self.host = host
        self.paths = []
        self.owner = owner

    def rm_paths(self):
        """ remove all paths of a rule """
        for path in self.paths:
            path.rm()


class kubelb:
    """ kubernetes load-balancer object """
    ingress_name = ''
    drivername = ''
    driver = ''
    resolvers_configured = False
    namespace = 'default'
    default_backend = '' # TODO: watch the default_backend's kube service
    # TLS configuration placeholder (TODO)
    rules = []
    last_update = ''

    def __init__(self, name, drivername):
        self.ingress_name = name
        self.drivername = drivername
        if self.drivername == 'haproxy':
            self.driver = haproxydriver()
        elif self.drivername == 'vtm':
            self.driver = vtmdriver()

    def dump_cur_conf(self):
        if self.default_backend:
            print('default_backend: %s' % self.default_backend)
        for rule in self.rules:
            print('host=%s' % rule.host)
            for path in rule.paths:
                print('    path=%s, serviceName: %s, servicePort: %s, serverResolution: %s' % (path.path, path.serviceName, path.servicePort, path.serverResolution))
                for server in path.servers:
                    print('        server=%s, IP=%s' % (server.serverName, server.serverIP))

    def add_rule(self, rl):
        if not self.rules:
            self.rules.append(rl)
            return

        for rule in self.rules:
            if rl.host == rule.host:
                return

        self.rules.append(rl)

    def rm_rule(self, rl):
        for rule in self.rules:
            if rl.host == rule.host:
                rl.rm_paths()
                self.rules.remove(rl)
                return


    def triggerLBUpdate(self, mylb):
        threadname = 'makeLBUpdate'
        if threadname in mylb.threads:
            return
        t = threading.Thread(target=self.makeLBUpdate, args=(mylb.conf_buffer, mylb.threads, mylb.lb.namespace))
        t.setName(threadname)
        mylb.threads[threadname] = t
        t.start()

    def makeLBUpdate(self, conf_buffer, threadlist, namespace):
        threadname = 'makeLBUpdate'
        while ((datetime.datetime.now() - self.last_update).total_seconds() < conf_buffer):
            time.sleep(1)
        self.driver.make_configuration(self.rules, '%s.svc.cluster.local' % (namespace) )
        #self.driver.print_configuration()
        del threadlist[threadname]


def watchEndpoints(ep_namespace, kpath, mylb):
    connection_refresh = False
    while True:
        api_instance = kubernetes.client.CoreV1Api()
        w = watch.Watch()
        for event in w.stream(api_instance.list_namespaced_endpoints, namespace=ep_namespace, timeout_seconds='13'):
            ep = event['object']

            # this event is not for us
            if ep.metadata.name != kpath.serviceName:
                continue

            # no need to ADD if we simply just reconnected
            if connection_refresh and event['type'] == 'ADDED':
                continue

            if mylb.debug:
                print('ENDPOINT: Event %s for %s' % (event['type'], ep.metadata.name))

            # for some reason, the Endpoint was improperly filled up
            if kpath.serverResolution == '':
                apiv1_instance = kubernetes.client.CoreV1Api()
                try:
                    svc = apiv1_instance.read_namespaced_service(kpath.serviceName, ep_namespace, pretty='true')
                    # if cluster_ip == None it means we can use DNS based resolution
                    # this can be used only if the LB is running inside kubernetes
                    if svc.spec.cluster_ip == 'None':
                        kpath.serverResolution = 'DNS'
                    else:
                        kpath.serverResolution = 'RC'
                        #kpath.servicePort = svc.
                except ApiException as e:
                    print("Exception when calling read_namespaced_service %s: %s\n" % (kpath.serviceName, e))
                    kpath.serverResolution = ''

            kpath.clear_serverlist()

            if not ep.subsets:
                continue

            for address in ep.subsets[0].addresses:
                serverid = len(kpath.servers)
                servername = 's%d' % serverid

    #            if kpath.serverResolution == 'RC':
    #            elif kpath.serverResolution == 'DNS':

                kpath.add_server(kubelbserver(servername, address.ip))

            mylb.lb.last_update = datetime.datetime.now()
            mylb.lb.triggerLBUpdate(mylb)

        if not kpath.serviceName:
            if mylb.debug:
                print("ENDPOINT: leaving obsolete thread")
            return

        connection_refresh = True


def watchIngresses(ing_name, mylb):
    api_instance = kubernetes.client.ExtensionsV1beta1Api()
    w = watch.Watch()
    for event in w.stream(api_instance.list_ingress_for_all_namespaces):
        ing = event['object']

        # check if the event is for our ingress
        if ing.metadata.name != ing_name:
            continue

        # check if the event is for our ingress driver
        if ing.metadata.annotations['kubernetes.io/ingress.class'] != mylb.lbtype:
            print('ERROR: wrong driver name: me: %s, event: %s' % (mylb.lbtype, ing.metadata.annotations['kubernetes.io/ingress.class']))
            continue

        if mylb.debug:
            print('INGRESS: Event %s for %s' % (event['type'], ing.metadata.name))

        if ing.spec.backend != 'None':
            mylb.lb.default_backend = ing.spec.backend
        
        for rule in ing.spec.rules:
            # we only support 'http' for now
            if not rule.http:
                continue

            host = rule.host
            ruleid = len(mylb.lb.rules)
            mylb.lb.add_rule(kubelbrule(host, mylb))

            for path in rule.http.paths:
                threadname = '%s_%s' % (ing_name, path.backend.service_name)
                # service already monitored for changes
                if threadname in mylb.threads:
                    continue

                servicePath = path.path
                serviceName = path.backend.service_name
                servicePort = path.backend.service_port

                pathid = len(mylb.lb.rules[ruleid].paths)
                mylb.lb.rules[ruleid].paths.append(kubelbpath(servicePath, serviceName, servicePort, mylb))
                mylb.lb.rules[ruleid].paths[pathid].owner = mylb

                apiv1_instance = kubernetes.client.CoreV1Api()
                try:
                    svc = apiv1_instance.read_namespaced_service(serviceName, mylb.lb.namespace, pretty='true')
                    # if cluster_ip == None it means we can use DNS based resolution
                    # this can be used only if the LB is running inside kubernetes
                    if svc.spec.cluster_ip == 'None':
                        mylb.lb.rules[ruleid].paths[pathid].serverResolution = 'DNS'
                    else:
                        mylb.lb.rules[ruleid].paths[pathid].serverResolution = 'RC'
                        #mylb.lb.rules[ruleid].paths[pathid].servicePort = servicePort
                except ApiException as e:
                    print("Exception when calling read_namespaced_service %s: %s\n" % (serviceName, e))
                    mylb.lb.rules[ruleid].paths[pathid].serverResolution = ''

                t = threading.Thread(target=watchEndpoints, args=(mylb.lb.namespace, mylb.lb.rules[ruleid].paths[pathid], mylb))
                t.setName(threadname)
                mylb.threads[threadname] = t
                t.start()

        # sanity check:
        # * remove rule(s) (hosts) which are obsolete (not configured anymore)
        for mylbrule in mylb.lb.rules:
            print(mylbrule.host)
            check = 'invalid'
            for rule in ing.spec.rules:
                if mylbrule.host == rule.host:
                    check = 'valid'
                    break
            if check == 'invalid':
                if mylb.debug:
                    print('cleanup rule: %s' % mylbrule.host)
                mylb.lb.rm_rule(mylbrule)

        # trigger the update
        mylb.lb.last_update = datetime.datetime.now()
        mylb.lb.triggerLBUpdate(mylb)
