from jinja2 import Environment
import subprocess

def get_resolvers():
    resolvers = []
    try:
        with open( '/etc/resolv.conf', 'r' ) as resolvconf:
            for line in resolvconf.readlines():
                if 'nameserver' in line:
                    resolvers.append( line.split()[ 1 ] )
        return resolvers
    except IOError as error:
        return error.strerror


class haproxydriver:
    """ HAProxy driver """
    haproxycfg = ''
    haproxydns_configured = False
    haproxyfrontend = ''
    haproxybackends = ''

    HAPROXY_GLOBAL = """
global
  nbproc 1
  daemon
  stats socket /haproxy.sock mode 660 level admin
  stats timeout 20m
  pidfile /haproxy.pid
"""

    HAPROXY_RESOLVERS = """
resolvers dns
  {% for item in resolvers %}nameserver ns{{ loop.index }} {{ item }}:53{% endfor %}
  timeout retry 1s
  hold valid 1s
  hold nx 10s
"""

    HAPROXY_DEFAULTS = """
defaults HTTP
  mode http
  timeout client 10s
  timeout server 30s
  timeout connect 4s
  option http-keep-alive
  option prefer-last-server
"""

    HAPROXY_STATS = """
frontend stats
  bind :8001
  mode http
  option forceclose
  stats enable
  stats uri /
  stats refresh 5
  stats show-desc HAProxy Kubernetes Controller
  stats show-legends
  stats show-node
"""

    HAPROXY_FRONTEND = """
frontend ft
  bind :80 name http"""

    HAPROXY_USE_BACKEND = """
  use_backend {{ backend_name }} if { hdr(Host) -i {{ host }} } { path_beg -i {{ path }} }
"""

    HAPROXY_BACKEND = """
backend {{ backend_name }}
{% for srv in servers %}  server {{ srv.serverName }} {{ srv.serverIP }}:{{ port }} check
{% endfor %}
"""

    HAPROXY_BACKEND_DNS = """
backend {{ backend_name }}
{% for srv in servers %}  server {{ srv.serverName }} {{ hostname }}:{{ port }} check resolvers dns
{% endfor %}
"""

    def __init__(self):
        self.haproxycfg = self.HAPROXY_GLOBAL + self.HAPROXY_DEFAULTS
        self.haproxyfrontend = self.HAPROXY_STATS + self.HAPROXY_FRONTEND
        self.haproxybackends = ''
        self.haproxydns_configured = False

    def reset_cfg(self):
        self.haproxydns_configured = False
        self.haproxycfg = self.HAPROXY_GLOBAL + self.HAPROXY_DEFAULTS
        self.haproxyfrontend = self.HAPROXY_STATS + self.HAPROXY_FRONTEND
        self.haproxybackends = ''

    def add_service(self, servicename, host, urlpath):
        self.haproxyfrontend = self.haproxyfrontend + Environment().from_string(self.HAPROXY_USE_BACKEND).render(backend_name=servicename, host=host, path=urlpath)

    def add_resolvers(self):
        if self.haproxydns_configured:
            return

        resolvers = get_resolvers()
        self.haproxycfg = self.haproxycfg + Environment().from_string(self.HAPROXY_RESOLVERS).render(resolvers=resolvers)
        self.haproxydns_configured = True

    def add_backend(self, service, servers, port):
        self.haproxybackends = self.haproxybackends + Environment().from_string(self.HAPROXY_BACKEND).render(backend_name=service, port=port, servers=servers)

    def add_backend_dns(self, service, fqdn, servers, port):
        self.add_resolvers()
        self.haproxybackends = self.haproxybackends + Environment().from_string(self.HAPROXY_BACKEND_DNS).render(backend_name=service, hostname=fqdn, servers=servers, port=port)

    def print_configuration(self):
        print(self.haproxycfg)
        print(self.haproxyfrontend)
        print(self.haproxybackends)

    def rm_backend(self, backend_name):
        return

    def make_configuration(self, rules, domain):
        self.reset_cfg()
        # TODO: don't forget default_backend

        for rule in rules:
            print('host=%s' % rule.host)

            for path in rule.paths:
                print('    path=%s, serviceName: %s, servicePort: %s, serverResolution: %s' % (path.path, path.serviceName, path.servicePort, path.serverResolution))
                self.add_service(path.serviceName, rule.host, path.path)

                print('    servers:')
                if path.serverResolution == 'DNS':
                    fqdn = '%s.%s' % (path.serviceName, domain)
                    self.add_backend_dns(path.serviceName, fqdn, path.servers, path.servicePort)
                elif path.serverResolution == 'RC':
                    self.add_backend(path.serviceName, path.servers, path.servicePort)

                for server in path.servers:
                    print('        server=%s, IP=%s' % (server.serverName, server.serverIP))

        haproxycfg = open('/haproxy.cfg', 'w', 0)
        haproxycfg.write(str(self.haproxycfg))
        haproxycfg.write('\n')
        haproxycfg.write(str(self.haproxyfrontend))
        haproxycfg.write('\n')
        haproxycfg.write(str(self.haproxybackends))
        haproxycfg.close()

        subprocess.call('/reload_haproxy.sh')
