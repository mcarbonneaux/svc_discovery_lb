from jinja2 import Environment
from pyvadc import Vtm, VtmConfig

import tempfile
import os

class vtmdriver:
    """ kubernetes driver for vtm """
    vserver = ''
    trafficscript = ''
    pools = []
    # API related configuration
    apiUrl = ''
    apiLogin = ''
    apiPassword = ''
    vtmConfig = ''
    vtm = ''

    VTM_TS = """#=-Perform HTTP based routing to services
$host = string.extractHost(string.lowercase(http.getHeader('Host')));
$path = string.lowercase(http.getPath());
"""

    VTM_TS_POOL_SELECT = """
if ( string.cmp($host, "{{ host }}") == 0 && string.startswith($path, "{{ path }}" ) ) {
  pool.use("{{ backend_name }}");
}
"""

    VTM_VSERVER = """
{
  "properties": {
    "basic": {
      "protocol": "http",
      "port": 80,
      "enabled": true,
      "listen_on_any": true,
      "request_rules": [
        "http_service_routing"
      ],
      "pool": "discard"
    }
  }
}
"""

    VTM_POOL = """
# {{ backend_name }}
{
  "properties": {
    "basic": {
      "nodes_table": [
{% for srv in servers %}        { "state": "active", "node": "{{ srv.serverIP }}:{{ port }}" }{% if not loop.last %},{% endif %}
{% endfor %}
      ],
      "monitors": [
        "Connect"
      ]
    }
  }
}
"""

    VTM_POOL_DNS = """
# {{ backend_name }}
{
  "properties": {
    "basic": {
      "monitors": [
        "Connect"
      ]
    },
    "dns_autoscale": {
      "enabled": true,
      "hostnames": [
        "{{ hostname }}"
      ],
      "port": {{ port }}
    }
  }
}
"""


    def __init__(self):
        self.trafficscript = self.VTM_TS
        self.vserver = self.VTM_VSERVER

    def init_vtm_api(self, url, login, password):
        self.apiUrl = url
        self.apiLogin = login
        self.apiPassword = password
        self.vtmConfig = VtmConfig(self.apiUrl, self.apiLogin, self.apiPassword)
        self.vtm = Vtm(self.vtmConfig)


    def reset_cfg(self):
        self.trafficscript = self.VTM_TS
        self.vserver = self.VTM_VSERVER


    def add_service(self, servicename, host, urlpath):
        self.trafficscript = self.trafficscript + Environment().from_string(self.VTM_TS_POOL_SELECT).render(backend_name=servicename, host=host, path=urlpath)


    def add_resolvers(self, resolvers):
        # TODO
        a = 1


    def add_backend(self, service, servers, port):
        self.pools.append(Environment().from_string(self.VTM_POOL).render(backend_name=service, port=port, servers=servers))


    def add_backend_dns(self, service, fqdn, servers, port):
        self.pools.append(Environment().from_string(self.VTM_POOL_DNS).render(backend_name=service, hostname=fqdn, servers=servers, port=port))


    def print_configuration(self):
        print self.trafficscript
        print self.vserver
        for pool in self.pools:
            print pool

    def rm_backend(self, backend_name):
        self.vtm.del_pool(backend_name)

    def make_configuration(self, rules, domain):
        self.reset_cfg()
        # TODO: don't forget default_backend

        for rule in rules:
            print('host=%s' % rule.host)

            for path in rule.paths:
                print('    path=%s, serviceName: %s, servicePort: %s, serverResolution: %s' % (path.path, path.serviceName, path.servicePort, path.serverResolution))
                self.add_service(path.serviceName, rule.host, path.path)

                print('    servers:')
                if path.serverResolution == 'DNS':
                    nodes = []
                    fqdn = '%s.%s' % (path.serviceName, domain)
                    nodes.append('%s:%s' % (fqdn, path.servicePort))
                    extra = '{"properties": {"dns_autoscale": {"enabled": true,"hostnames": ["%s"], "port": %s } } }' % (fqdn, path.servicePort)
                    self.vtm.add_pool(path.serviceName, nodes, None, None, ['Connect'], extra=extra)
                elif path.serverResolution == 'RC':
                    nodes = []
                    for srv in path.servers:
                        nodes.append('%s:%s' % (srv.serverIP, path.servicePort))
                    self.vtm.add_pool(path.serviceName, nodes, None, None, ['Connect'])

                for server in path.servers:
                    print('        server=%s, IP=%s' % (server.serverName, server.serverIP))

        tsfilefd, tsfilename = tempfile.mkstemp(text=True)
        tmp = os.fdopen(tsfilefd, 'w')
        tmp.write(self.trafficscript)
        tmp.close()

        self.vtm.upload_rule('ts_kuberules', tsfilename)
        os.unlink(tsfilename)

        self.vtm.add_vserver('vs_kubernetes_services', 'discard', '', 80, 'http', extra='{ "properties": { "basic": { "listen_on_any": true } } }')
        rules = self.vtm._get_vs_rules('vs_kubernetes_services')
        if 'ts_kuberules' not in rules["request_rules"]:
            self.vtm.insert_rule('vs_kubernetes_services', 'ts_kuberules')
